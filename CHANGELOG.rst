CHANGELOG
=========

0.5.0: 2020/12/23
-----------------
* Add tqdm package to requirements.txt and setup.py
* Core:
    * Dataset:
        * Update constructor to admit `first_filename` parameter. (MR !106)
        * Fix reshaping: Correct the methodology used for reshaping to be consistent with the way data is obtained in id06 (MR !101)
        * Modify methods that work with dimensions (get_data, apply_shift) so that they work if more than one dimension is fixed (MR !102)
        * Add `running_data` property which contains the data currently being used by any of the operations (if any). This data is stopped in case of `Abort` option called (MR !95)
        * apply_background_subtraction:
            * Use multiprocessing to chunk images (MR !87)
            * Add step option to compute median using only every step image (MR !87)
            * Add tests with step and chunks and add docstring (MR !92)
        * Add `apply_moments` method for computing orientation distribution and colorkey based on the dimensions (for now only works with two dimensions) (MR !96)
        * Add `apply_fit` method for fitting the rocking curves of the data (a curve corresponds to a pixel around the stack), multiprocessing is used to chunk the images (MR !98)
        * Add `apply_threshold` method for thresholding the data (MR !108)
    * Create new file `dimension.py` that contains the classes AcquisitionDims and Dimensions moved from `dataset.py` (MR !105)
    * Create new file `mapping.py` with several functions: fit a rocking curve and computation of moments (MR !98)
    * `process.py`: Add new process `_RockingCurves` (MR !104)
    * Add `test_dimension.py` file that contains the tests that use the Dimension and AcquisitionDim classes (MR !105)
* GUI:
    * Add setters and getters for colormaps in widgets that use them (MR !110)
    * Disable buttons when computing operations (MR !109)
    * `datasetSelectionWidget.py` (MR !106)
        * Add new tab for treated data.
        * Create class `DirSelectionWidget`
    * `noiseRemovalWidget.py`:
        * Add `setDataset` method in `NoiseRemovalDialog` to enable buttons after the dataset is set (MR !100)
        * Add widgets only visible when data in disk, which give option to use step or chunks when computing the median (MR !87, MR!90)
        * Fix bug when showing or not the in disk widget (MR !97)
        * Make methods `toggleChunks` and `toggleInDiskWidget` private (MR !100)
        * Correctly set and get properties (background, method, etc) (MR !104)
        * Add threshold removal option (MR !108)
        * Add signal that emits when method starts or ends computing (MR !109)
        * Fix some typos (MR !111)
    * `shiftCorrectionWidget.py`:
        * Use `skimage.registration.phase_correlation` instead of `register_translation` when finding shift (MR !91)
        * Fix bug when shifting only using first dimension (MR !99)
        * Round shift to 5 decimals (MR !105)
    * `roiSelectionWidget.py`:
        * Run ROI operation in thread and add abort button(MR !95)
    * `blindSourceSeparationWidget.py`:
        * Modify `Method` class so that its values are a description of the method. The keys are used in the `BlindSourceSeparationWidget` as text in the combobox, and the values are its tooltips (MR !100)
        * Remove widgets to set a maximum number of components (MR !105)
    * `pca.py` (MR !105):
        * Add title and labels to plot
        * Automatically compute PCA when creating widget and remove widgets to set a maximum number of components
    * `displayComponentsWidget.py`:
        * Add parameter in `setDimensions` to know the shape of the data, and use it to correctly take values from W (MR !107)
    * `ChooseDimensionWidget`: Add as many dimension widgets as dimensions are (minus 1), so all the dimensions are fixed except for one (MR !102)
    * Create `GrainPlotWidget`:
        * Show different COM, FWHM, Skewness, Kurtosis of the dataset around a certain dimension (MR !96)
        * Add a contours map to show the contours of the orientation distribution on top of the colorkey (MR !96)
        * Add a plot for the mosaicity map (only works with two dimensions) (MR !96)
    * Create `RockingCurvesWidget` (MR !98, MR !111, MR !112):
        * Show the stack of images where the user can click any pixel.
        * Show a 1D plot with the rocking curve of the pixel selected in the stack.
        * A ChooseDimensionWidget allows the data to be filtered by the chosen dimension.
        * A button to fit the data shows a plot with 3 maps computed from the fitted data: Integrated intensity, FWHM and peak position.
        * If data is filtered by dimensions then the data is fitted in chunks, where every chunk is a certain value of the chosen dimension.
    * Create `LineProfileWidget` (MR !103):
        * Upload a *.npy image from disk and show intensity curve from a chosen y value of the image.
    * Create `ShowStackWidget` to show stack of data (MR !106)
* pffaddon: Add alias for rockingCurveWidget (MR !104)
* orangecontrib:
    * Zsum: Use `ChooseDimensionDock` instead of `ChooseDimensionWidget` and fix bug when computing sum in disk (MR !92)
    * Add `GrainPlotWidgetOW` to show a `GrainPlotWidget` (MR !96)
    * Add `RockingCurvesWidgetOW` to show a `RockingCurvesWidget` (MR !98)
    * Add `LineProfileWidgetOW` for `LineProfileWidget` (MR !103)
    * Add properties in `NoiseRemovalWidgetOW` for threshold removal (MR !108)
    * Add colormap as input and output on widgets that use it (MR !110)

0.4.2: 2020/09/15
-----------------
* Use version 4.1.2.30 of opencv to avoid uncompatibility with PyQt5 latest release

0.4.1: 2020/09/14
-----------------
* Modify `setup.py` (MR !82):
    * Add `opencv-python`, `scikit-image` and `silx` in `install_requires`.
    * Add package `full` in `extras` with `matplotlib`, `PyQt5` and `orange`.
    * Add package `test` in `extras` with `pillow`.
* Core:
    * `dataset.py`:
        * Add parameter bins when partitioning data in `Dataset` (MR !67)
        * Create `in_memory` setter in `Dataset` (MR !68)
        * Improve methods `flatten`, `reshape` and `take` in `Data` (MR !67)
        * Add try-except when fitting data in blind source separation methods to close file in case of crash (MR !71)
        * Add methods to convert dimensions to dictionary and viceversa in `AcquisitionDims` (MR !74)
        * Add `to_memory` method in `Dataset` to load only part of the data into memory (MR !77)
        * Add property `ndim` in `Data` (MR !80)
* Decomposition:
    * Fix bug in IPCA with cases where in the last iteration the chunk was smaller than the number of components (omit last iteration) (MR !73)
* GUI:
    * Temporarily register resources directory in when used in `DisplayComponentsWidget` (MR !84)
    * Change colormap default normalization to `log` for better visualization of the data (MR !85)
    * Add widget for bins in `DataPartitionWidget` (MR !67)
    * Add parameter for chunks in `NoiseRemovalWidget` (MR !68)
    * Add abort options in `dataPartitionWidget`, `NoiseRemovalWidget` and `ShiftCorrectionWidget` (MR !68)
    * Get next free axis when removing dimensions in `DimensionsWidget (MR !86)
    * Fix bug when clearing stack in `RoiSelectionWidget` (MR !67)
    * Fix bug when getting filtered indices in `DataPartitionWidget` (MR !68)
* IO:
    * New file `dataset_io.py` with functions to save and load data into/from json files (MR !74)
* Orangecontrib:
    * Remove progressBar due to bugs in the workflows (MR !84)
    * Fix bug with qthreads when creating and deleting the thread using orange widgets (MR !70)

0.4.0: 2020/05/15
-----------------
* Refactors the project (MR !48)
    * Changes default colormap to cividis.
    * Core:
        * Dataset class stops inheriting from `silx.qt.QObject`
        * The dataset class takes control of all the operations done to the data.
        * Metadata is obtained from the frames using `fabioh5` from silx and then each frame is closed to spare space in memory.
        * Creates new class `Data` that inherits from numpy.ndarray and contains the corresponding urls and metadata of the data.
        * Adds new methods to `Dataset` for image operations, image registration, region of interest, and partition by intensity (filter data), that are applied differently depending on if the data is loaded into memory or is taken from disk in chunks.
        * Data can be saved into an Hdf5 file with images in the rows.
        * Implement in `Data` methods `shape`, `reshape`, `flatten`, `take`, `__getitem__` that work similarly to a numpy.ndarray.
        * Adds and modifies functions in `imageOperations.py` and `imageRegistration.py` to be used when data  is not loaded into memory.
        * Adds class `IPCA` in `blindSourceSeparation.py` that uses IncrementalPCA from scikit-learn to apply PCA in chunks.
        * Implements methods in `Dataset` to apply blind source separation algorithms to the data.
        * PCA and NMF algorithms used from scikit-learn when data is loaded into memory.
    * Decomposition:
        * A new package `decomposition` is created.
        * A base class `Base` is created for decomposition methods.
        * Adds class `NMF` that updates the matrices `H` and `W` using NMF method without having the data in memory.
        * Adds class `NICA` with the already implemented algorithm in `core`.
        * Adds class `PCA` currently not used (substitued for PCA in scikit-learn).
    * GUI:
        * Modifies all widgets to only have `Dataset` objects and call its corresponding methods.

* Core:
    * Adds method `in_memory` to Dataset to upload data from disk or remove data from memory (MR !51)
    * Adds method that returns intensity per frame (MR !58)
    * Number of bins can be passed as parameter when partitioning the data by intensity (MR !52)
    * Fix bugs when not closing Hdf5 files (MR !56)
    * Adds methos `zsum` in `Dataset` to sum images when not in memory (MR !57)
    * Fix bugs with `__getitem__` in `Data` (MR !57)

* Decomposition:
    * Adds IPCA to decomposition package (MR !50)
    * NICA implements IPCA for whitening when data is not in memory. Fix bugs when computing IPCA with data in the rows or in the columns (MR !53)
    * Allow matrices H and W in IPCA to be stored in disk using Hdf5 (MR !54)

* GUI:
    * Adds ComboBox in `DatasetSelectionWidget` to use data from disk or load it into memory (MR !57)
    * Adds widget to partition the data by intensity (MR !58)
    * Removes ComboBox in `DatasetSelectionWidget` to filter the data (MR !59)

* Orangecontrib:
    * Add corresponding data partition widget (MR !58)
    * Update example tutorial (MR !64)

0.3.0: 2020/03/03
-----------------
* Adds resources package (MR !29)
* Adds configuration file for default parameters (MR !30)
* Minor fixes (MR !12 !14 !15 !32 !37 !41 !46)
* Use silx backend when available (MR !44)
* Core:
    * Adds tolerance to dimensions that defines the uniqueness of its values (MR !17)
    * Modifies ROI performance (MR !23)
    * Implements blind source separation algorithms (MR !18 !20 !22 !28)
    * Implements algorithms for linking components between datasets: ORB, SIFT, Euclidean distance (MR !36 !44 !45)
* GUI:
    * Creates a unified noise removal widget (MR !21 !24 !25)
    * Adds checkbox to filter the data in `DataSelectionWidget` (MR !19)
    * Improves `DimensionWidget` (MR !16 !17 !33)
    * Improves `roiSelectionWidget` (MR !23 !31 !43)
    * Implements widgets `blindSourceSeparationWidget` and `displayComponentsWidget` to display the components (MR !18 !22 !26 !29 !35)
    * Creates `PCAWidget` to show the eigenvalues of the PCA computation (MR !22)
    * Creates widget `LinkComponentsWidget` that implements the component matching algorithms from two sets of components (MR !36 !38)
* IO:
    * Creates functions for reading and writing the components of the BSS into Nexus files (MR !42)

0.2.0: 2019/10/15
-----------------
* Renames project linkdick06 to darfix
* Refactors project into core, gui, io and test packages
* Adds unittests
* Adds orangecontrib package
* Adds examples package
* Adds continous integration with gitlab-ci
* Adds requirements.txt file with dependencies on numpy, silx, PyQt5, opencv-python, scikit-image
  and orange3 with version 3.22.0
* Core:
    * Dataset class inherits from `silx.qt.QObject`
    * Adds threshold to split data into high intensity images and low intensity images
    * Adds `Dimension` class to save information about how the frames are taken through the dataset
    * Adds roi file that implements region of interest functions
    * Image registration operations moved to new file `imageRegistration.py`
    * Adds shift detection using OpenCV
    * Adds `autofocus.py` to use at the shift detection
    * Adds `geneticShiftDetection` file to improve shift detection using a genetic algorithm
* GUI:
    * Adds `RoiSelectionWidget`
    * Adds `ShiftCorrectionWidget` to apply shift detection and shift correction
    * Adds widgets `BackgroundSubstractionWidget` and `HotPixelRemovalWidget` for noise removal
    * Adds widgets for data selection, metadata, and choosing dimensions
    * Adds a `QThread`, `operationThread`, to run widgets operations in a thread
    * Adds utils to create datasets from scratch
    * Removes `frameInterpretationWidget` and adds `DimensionWidget`
* IO:
    * Adds utils

0.1.0: 2019/07/11
-----------------
* Adds project build, documentation and test structure
* Adds continuous integration set-up for Gitlab
* Adds EDF reader from Fabio
* Adds operations for correction of images in imageOperations
* Adds notebook tutorial