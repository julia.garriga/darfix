aliases = {
    # define aliases for control
    'orangecontrib.darfix.widgets.roiselection.RoiSelectionWidgetOW': 'darfix.core.process._ROI',
    'orangecontrib.darfix.widgets.noiseremoval.NoiseRemovalWidgetOW': 'darfix.core.process._NoiseRemoval',
    'orangecontrib.darfix.widgets.dataselection.DataSelectionWidgetOW': 'darfix.core.process.IgnoreProcess',
    'orangecontrib.darfix.widgets.dimensions.DimensionWidgetOW': 'darfix.core.process._DimensionDefinition',
    'orangecontrib.darfix.widgets.datapartition.DataPartitionWidgetOW': 'darfix.core.process._DataPartition',
    'orangecontrib.darfix.widgets.shiftcorrection.ShiftCorrectionWidgetOW': 'darfix.core.process._ShiftCorrection',
    'orangecontrib.darfix.widgets.blindsourceseparation.BlindSourceSeparationWidgetOW': 'darfix.core.process._BlindSourceSeparation',
    'orangecontrib.darfix.widgets.rockingcurves.RockingCurvesWidgetOW': 'darfix.core.process._RockingCurves',
}
