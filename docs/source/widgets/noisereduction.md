noise reduction
==============

![image](icons/noise_removal.png)

Widget to reduce the noise from a dataset raw data given a set of dark frames.

Signals
-------

- Dataset

**Outputs**:

- Dataset

Description
-----------

TODO