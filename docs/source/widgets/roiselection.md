roi selection
=============

![image](icons/image-select-box.png)

Region of Interest selection widget

Signals
-------

- Dataset

**Outputs**:

- Dataset

Description
-----------

ROI selection widget that accepts a dataset. Returns the dataset reduced to the selected ROI.
