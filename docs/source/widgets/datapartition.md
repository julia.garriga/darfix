data partition
==============

![image](icons/filter.png)

Widget to divide the dataset in high intensity and low intensity images.

Signals
-------

- Dataset

**Outputs**:

- Dataset

Description
-----------

TODO