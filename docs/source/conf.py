import os
import sys

project = 'darfix'

try:
    import darfix
    project_dir = os.path.abspath(os.path.join(__file__, "..", "..", ".."))
    build_dir = os.path.abspath(darfix.__file__)
    if not build_dir.startswith(project_dir):
        raise RuntimeError("%s looks to come from the system. Fix your PYTHONPATH and restart sphinx." % project)
except ImportError:
    raise RuntimeError("%s is not on the path. Fix your PYTHONPATH and restart sphinx." % project)


# Add local sphinx extension directory

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ext'))
templates_path = ['templates']

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx.ext.doctest',
    'nbsphinx',
    'recommonmark',

]

copyright = '2019-2020 European Synchrotron Radiation Facility'
author = 'J.Garriga'
version = ''
release = '0.5.0'
templates_path = ['_templates']
source_suffix = ['.rst', '.md']
master_doc = 'index'
pygments_style = 'sphinx'
html_theme = 'classic'
# Output file base name for HTML help builder.
htmlhelp_basename = 'darfix'
# source_parsers = {".md": "recommonmark.parser.CommonMarkParser"}
