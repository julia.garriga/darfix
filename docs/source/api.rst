API
---

Dataset
=======

.. automodule:: darfix.core.dataset
    :members:

Dimension
=========

.. automodule:: darfix.core.dimension
    :members:

Image Operations
================

.. automodule:: darfix.core.imageOperations
    :members:

Image Registration
==================

.. automodule:: darfix.core.imageRegistration
    :members:

Genetic Shift Detection
=======================

.. automodule:: darfix.core.geneticShiftDetection
    :members:

Region of Interest
==================

.. automodule:: darfix.core.roi
    :members:

Components Matching
=======================

.. automodule:: darfix.core.componentsMatching
    :members:

Mapping
=======

.. automodule:: darfix.core.mapping
    :members:

Decomposition
=============

.. automodule:: darfix.decomposition.base
    :members:


.. automodule:: darfix.decomposition.ipca
    :members:


.. automodule:: darfix.decomposition.nica
    :members:


.. automodule:: darfix.decomposition.nmf
    :members:

Process
=======

.. automodule:: darfix.core.process
    :members:


GUI
===
.. automodule:: darfix.gui.datasetSelectionWidget
    :members:

.. automodule:: darfix.gui.metadataWidget
    :members:

.. automodule:: darfix.gui.dimensionsWidget
    :members:

.. automodule:: darfix.gui.dataPartitionWidget
    :members:

.. automodule:: darfix.gui.shiftCorrectionWidget
    :members:

.. automodule:: darfix.gui.roiSelectionWidget
    :members:

.. automodule:: darfix.gui.noiseRemovalWidget
    :members:

.. automodule:: darfix.gui.blindSourceSeparationWidget
    :members:

.. automodule:: darfix.gui.PCAWidget
    :members:

.. automodule:: darfix.gui.displayComponentsWidget
    :members:

.. automodule:: darfix.gui.linkComponentsWidget
    :members:

.. automodule:: darfix.gui.grainPlotWidget
    :members:

.. automodule:: darfix.gui.rockingCurvesWidget
    :members:

.. automodule:: darfix.gui.lineProfileWidget
    :members:

.. automodule:: darfix.gui.showStackWidget
    :members:

.. automodule:: darfix.gui.utils
    :members:
