
Installation steps
==================


Installing with pip
-------------------

To install darfix (and all its dependencies_), run:

.. code-block:: bash

 	pip install -r requirements.txt

.. code-block:: bash

	pip install .



Dependencies
------------

.. _dependencies:

The mandatory dependencies are:

- `numpy <http://www.numpy.org/>`_
- `silx <https://github.com/silx-kit/silx>`_
- `fabio <https://github.com/silx-kit/fabio>`_

The GUI widgets depend on the following extra packages:

* A Qt binding: either `PyQt5 <https://riverbankcomputing.com/software/pyqt/intro>`_,
  or `PySide2 <https://wiki.qt.io/Qt_for_Python>`_
* `matplotlib <http://matplotlib.org/>`_

Tools for reading and writing files depend on the following packages:

* `h5py <http://docs.h5py.org/en/latest/build.html>`_ for HDF5 files
* `fabio <https://github.com/silx-kit/fabio>`_ for multiple image formats