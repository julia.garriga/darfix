=============
Darfix
=============

The purpose of this API is to create a set of functions and classes to help the beamline ID06 HXRM read a series of data images and apply the operations needed for the experiments.

.. toctree::
   :hidden:

   api.rst
   tutorials.rst
   install.rst
   changelog.rst
   license.rst

:doc:`install`
    How to install *darfix* on Linux, Windows and MacOS X

:doc:`tutorials`
    Tutorials and sample code

:doc:`changelog`
    List of changes between releases

:doc:`license`
    License and copyright information

:doc:`api`
    API


Widgets
-------

.. toctree::
   :maxdepth: 1

   widgets/roiselection
   widgets/dataselection
   widgets/shiftcorrection
   widgets/noisereduction
   widgets/datacopy
   widgets/blindsourceseparation
   widgets/datapartition

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

